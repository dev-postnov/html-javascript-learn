(function() {

    var form = document.querySelector('.form'),
        parent = document.querySelector('.images'),
        photoTemplate = document.querySelector('#photo-template').innerHTML,
        queue = [];




    form.addEventListener('submit', function(e) {
        e.preventDefault();


        var formData = new FormData(form);


        queue.forEach(function(element) {
            formData.append('images', element.file)
        })


        request(formData, function(response) {
            console.log(response);
        })

    })


    document.querySelector('#photos').addEventListener('change', function() {
        var files = this.files;

        for(var i = 0; i < files.length; i++) {
            preview(files[i]);
        }

        this.value = '';
    })


    function preview(file) {


        if (file.type.match(/image.*/)) {
            var reader = new FileReader();

            var img = document.createElement('img');

            reader.addEventListener('load', function (event) {

                var html = Mustache.render(photoTemplate, {
                    "image": event.target.result,
                    "name": file.name
                })

                var li = document.createElement('li');
                li.classList.add('images__item');
                li.innerHTML = html;

                parent.appendChild(li);

                li.querySelector('.images__delete').addEventListener('click', function(e) {
                    e.preventDefault();
                    removePreview(li);
                })


                queue.push({ file: file, li: li });
            })






            reader.readAsDataURL(file)
        }
    }


    //delete photo

    function removePreview(li) {
        queue = queue.filter(function(element) {
            return element.li != li;
        })

        li.parentNode.removeChild(li);
    }










    // ajax request
    function request(data, fn) {
        var xhr = new XMLHttpRequest();

        xhr.open('POST', 'https://httpbin.org/post');

        xhr.addEventListener('readystatechange', function () {
            if (xhr.readyState == 4) {
                fn(xhr.responseText);
            }
        })

        xhr.send(data)
    }


})()